<?php 
    // Template Name: Home Page
?>

<?php get_header(); ?>
    <main>
        <div id="main_div1">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-intro.jpg" alt="Canecas de café">
            <div id="main_div2">
                <h1>CAFÉS COM A CARA<br>
                    DO BRASIL<br>
                    -
                </h1> 
                <p id="main_p">Direto das fazendas de Minas Gerais</p>
            </div>
        </div>
    </main>
    
    <section>
        <h2>Uma Mistura de</h2>
        <div class="flex_center">
            <div class="sec1_div1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg" alt="xicara com coração">
                <div class="sec1_div2">amor</div>
            </div>
            <div class="sec1_div1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg" alt="xicara de café">
                <div class="sec1_div2">perfeição</div>
            </div>
        </div>
        <p id="sec1_p">O café é uma bebida produzida a partir dos grãos torrados do fruto do cafeeiro. É servido tradicionalm aente quente, mas também pode ser consumido gelado. Ele é um estimulante, por possuir cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do método de preparação.
        </p>
    </section>
    
    <section class="section">
        <div class="flex_center container">
            <div class="sec2_div1">
                <div class="sec2_div2 cor_paulista">
                    <button class="sec2_button cor_paulista" ></button>
                </div>
                <h3>Paulista</h3>
                <p class="sec2_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                </p>
            </div>                
            <div class="sec2_div1">
                <div class="sec2_div2 cor_carioca">
                    <button class="sec2_button cor_carioca"></button>
                </div>
                <h3>Carioca</h3>
                <p class="sec2_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                </p>
            </div>
            <div class="sec2_div1">
                <div class="sec2_div2 cor_mineiro">
                    <button class="sec2_button cor_mineiro"></button>
                </div>                
                <h3>Mineiro</h3>
                <p class="sec2_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
                </p>
            </div>
        </div>
        <button class="button">SAIBA MAIS</button>
    </section>

    <section>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="loja botafogo">
            </div>
            <div class="sec3_div">
                <h3>Botafogo</h3>
                <p class="sec3_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.
                </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="loja iguatemi">
            </div>
            <div class="sec3_div">
                <h3>Iguatemi</h3>
                <p class="sec3_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.
                </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="loja mineirão">
            </div>
            <div class="sec3_div">
                <h3>Mineirão</h3>
                <p class="sec3_p">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.
                </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
    </section>

    <section class="section">
        <div id="sec4_div" class="container">
            <div>
                <h3>Assine Nossa Newsletter</h3>
                <h4>promoções e eventos mensais</h4>
            </div>
            <form>
                <input type="email" name="e-mail" placeholder="Digite seu e-mail"  id="">
                <input type="submit" value="ENVIAR">
            </form>
        </div>
    </section>

<?php get_footer(); ?>